package database

import "time"

//User is used to represent user information
type User struct {
	Email            string `xorm:"unique not null"`
	Password         string `xorm:"not null"`
	FirstName        string
	LastName         string
	Address          string
	Telephone        string
	RegistrationDate time.Time `xorm:"created"`
}
