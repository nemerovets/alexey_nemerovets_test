package database

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
)

//DatabaseEngine is used for database connection
var DatabaseEngine *xorm.Engine

//Connect creates a connection to a db and returns ORM engine or error
func Connect() error {
	var err error
	DatabaseEngine, err = xorm.NewEngine("mysql", "testUser:testPwd@tcp(127.0.0.1:3306)/testDb")
	if err != nil {
		return err
	}

	return nil
}
