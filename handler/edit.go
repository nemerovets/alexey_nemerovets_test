package handler

import (
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

//EditHandler provides edit user info form
func EditHandler(w http.ResponseWriter, r *http.Request) {
	user := helper.GetCurrentUser(r)
	if r.Method == http.MethodGet {
		helper.WriteTemplate(w, r, "template/edit.html", *user)
		return
	}
	if user == nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	updatedUser := *user
	updatedUser.FirstName = r.FormValue("firstName")
	updatedUser.LastName = r.FormValue("lastName")
	updatedUser.Address = r.FormValue("address")
	updatedUser.Telephone = r.FormValue("telephone")
	_, err := database.DatabaseEngine.Update(&updatedUser, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	err = helper.LogIn(r, w, updatedUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	http.Redirect(w, r, "/profile", http.StatusFound)
}
