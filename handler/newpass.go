package handler

import (
	"log"
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

//NewPassHandler resets user password
func NewPassHandler(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get("key")
	storedKey := helper.CurrentRestorationKey(r)
	if r.Method == http.MethodGet {
		if key != storedKey {
			log.Printf("Session key: %s, params key: %s", storedKey, key)
			http.Error(w, "Wrong key", http.StatusBadRequest)
			return
		}
		helper.WriteTemplate(w, r, "template/newpass.html", nil)
		return
	}

	newPass := r.FormValue("password")
	user := helper.GetCurrentUser(r)
	updatedUser := *user
	updatedUser.Password = newPass
	_, err := database.DatabaseEngine.Update(&updatedUser, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	helper.LogIn(r, w, *user)
	http.Redirect(w, r, "/", http.StatusFound)
}
