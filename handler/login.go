package handler

import (
	"log"
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

//LoginHandler provides login form
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		if helper.LoggedIn(r) {
			http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
		}
		helper.WriteTemplate(w, r, "template/login.html", nil)
		return
	}
	email := r.FormValue("email")
	password := r.FormValue("password")
	user := database.User{}
	user.Email = email
	user.Password = password
	exists, err := database.DatabaseEngine.Get(&user)
	log.Println(exists, err)
	if exists && err == nil {
		err := helper.LogIn(r, w, user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		http.Redirect(w, r, "/profile", http.StatusFound)
	} else {
		http.Error(w, "Wrong email or password", http.StatusBadRequest)
	}

}
