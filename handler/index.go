package handler

import (
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

// IndexHandler handles main page's requests
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	loggedIn := helper.LoggedIn(r)
	if !loggedIn {
		helper.WriteTemplate(w, r, "template/index.html", nil)
	} else {
		http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
	}
}
