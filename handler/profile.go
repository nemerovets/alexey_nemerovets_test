package handler

import (
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

//ProfileHandler shows user information
func ProfileHandler(w http.ResponseWriter, r *http.Request) {
	if !helper.LoggedIn(r) {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	usr := helper.GetCurrentUser(r)
	if usr == nil {
		usr = &database.User{}
	}
	helper.WriteTemplate(w, r, "template/profile.html", *usr)
}
