package handler

import (
	"fmt"
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/mail"
)

//ResetHandler shows password reset form
func ResetHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		helper.WriteTemplate(w, r, "template/reset.html", nil)
		return
	}
	email := r.FormValue("email")

	if len(email) > 0 {
		key := helper.GenerateKey(10)
		helper.SetRestorationKey(r, w, key)
		link := fmt.Sprintf("http://127.0.0.1:8080/newpass?key=%s", key)
		user := database.User{}
		user.Email = email
		err := helper.LogIn(r, w, user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		mail.ResetEmail(email, link)
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
