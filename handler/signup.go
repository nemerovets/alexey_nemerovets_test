package handler

import (
	"net/http"
	"time"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

//SignupHandler provides registration form
func SignupHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		helper.WriteTemplate(w, r, "template/signup.html", nil)
		return
	}
	email := r.FormValue("email")
	password := r.FormValue("password")
	user := database.User{}
	user.Email = email
	user.Password = password
	user.RegistrationDate = time.Now()
	_, err := database.DatabaseEngine.Insert(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = helper.LogIn(r, w, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	http.Redirect(w, r, "/edit", http.StatusFound)
}
