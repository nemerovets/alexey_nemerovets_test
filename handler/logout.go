package handler

import (
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/helper"
)

//LogoutHandler is used to mark user as logged out
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	helper.LogOut(r, w)
}
