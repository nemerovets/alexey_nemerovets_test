package main

import (
	"log"
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"
	"bitbucket.org/nemerovets/alexey_nemerovets_test/handler"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", handler.IndexHandler).Methods(http.MethodGet)
	router.HandleFunc("/login", handler.LoginHandler).Methods(http.MethodGet, http.MethodPost)
	router.HandleFunc("/signup", handler.SignupHandler).Methods(http.MethodGet, http.MethodPost)
	router.HandleFunc("/profile", handler.ProfileHandler).Methods(http.MethodGet)
	router.HandleFunc("/edit", handler.EditHandler).Methods(http.MethodGet, http.MethodPost)
	router.HandleFunc("/logout", handler.LogoutHandler).Methods(http.MethodGet)
	router.HandleFunc("/reset", handler.ResetHandler).Methods(http.MethodGet, http.MethodPost)
	router.HandleFunc("/newpass", handler.NewPassHandler).Methods(http.MethodGet, http.MethodPost)
	err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}
	http.ListenAndServe(":8080", router)
}
