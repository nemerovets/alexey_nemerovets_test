package helper

import (
	"html/template"
	"net/http"
)

//WriteTemplate is used to write template in response
func WriteTemplate(w http.ResponseWriter, r *http.Request, templatePath string, templateData interface{}) {
	tmpl, err := template.ParseFiles(templatePath)
	if err != nil {
		//For debuging
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = tmpl.Execute(w, templateData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
