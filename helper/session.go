package helper

import (
	"encoding/gob"
	"errors"
	"log"
	"net/http"

	"bitbucket.org/nemerovets/alexey_nemerovets_test/database"

	"github.com/gorilla/sessions"
)

var store sessions.Store

func init() {
	store = sessions.NewCookieStore([]byte("test-secret-key"))
	gob.Register(&database.User{})
}

func getSession(r *http.Request, name string) *sessions.Session {
	session, err := store.Get(r, name)
	if err != nil {
		log.Println(err.Error())
		return nil
	}
	if session == nil {
		session, _ = store.New(r, name)
	}
	return session
}

//LoggedIn checks if user logged in or not
func LoggedIn(r *http.Request) bool {
	session := getSession(r, "testCookie")
	if session == nil {
		return false
	}

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		log.Printf("Auth %v, exists: %v", auth, ok)
		return false
	}
	return true
}

//LogIn is used for marking user as authenticated
func LogIn(r *http.Request, w http.ResponseWriter, user database.User) error {
	session := getSession(r, "testCookie")
	if session == nil {
		return errors.New("Error logging in")
	}
	if len(user.Password) > 0 {
		session.Values["authenticated"] = true
	}
	session.Values["user"] = user
	err := session.Save(r, w)
	if err != nil {
		log.Fatal(err.Error())
	}
	return nil
}

//GetCurrentUser returns current session user info
func GetCurrentUser(r *http.Request) *database.User {
	session := getSession(r, "testCookie")
	if session == nil {
		return nil
	}
	val := session.Values["user"]
	if user, ok := val.(*database.User); ok {
		log.Println(*user)
		return user
	}
	return nil
}

//SetRestorationKey saves password restoration key to the session
func SetRestorationKey(r *http.Request, w http.ResponseWriter, key string) {
	session := getSession(r, "testCookie")
	session.Values["key"] = key
	session.Save(r, w)
}

//CurrentRestorationKey is used to get current key from session
func CurrentRestorationKey(r *http.Request) string {
	session := getSession(r, "testCookie")
	key := session.Values["key"].(string)
	return key
}

//LogOut is used for de-authenticate user
func LogOut(r *http.Request, w http.ResponseWriter) error {
	session, err := store.Get(r, "testCookie")
	if err != nil {
		return err
	}
	session.Values["authenticated"] = false
	sessions.Save(r, w)
	http.Redirect(w, r, "/", http.StatusFound)
	return nil
}
