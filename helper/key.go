package helper

import (
	"math/rand"
	"time"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

//GenerateKey returns a string of random letters
func GenerateKey(length int) string {
	rand.Seed(time.Now().Unix())
	result := make([]byte, length)
	for i := range result {
		result[i] = letters[rand.Int63()%int64(len(letters))]
	}
	return string(result)
}
