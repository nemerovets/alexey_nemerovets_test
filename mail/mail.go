package mail

import (
	"crypto/tls"
	"fmt"

	"gopkg.in/gomail.v2"
)

//ResetEmail sends message with password restoration link
func ResetEmail(to, resetLink string) {
	m := gomail.NewMessage()
	m.SetHeader("From", "testapp@nmrvtz.com")
	m.SetHeader("To", to)
	m.SetHeader("Subject", "New password")
	messageBody := fmt.Sprintf("Visit <a href=\"%s\">this</a> link to reset your password", resetLink)
	m.SetBody("text/html", messageBody)

	d := gomail.NewDialer("smtp.gmail.com", 587, "nmrvtz@gmail.com", "testpass")
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}
}
